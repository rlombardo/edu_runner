var fs = require('fs')

const VERSION_PATH = 'src/config/version.json'
const version = new Date().getTime()
const versionData = { version }

if (fs.existsSync(VERSION_PATH)) fs.unlinkSync(VERSION_PATH)
const output = JSON.stringify(versionData)
fs.writeFileSync(VERSION_PATH, output, 'utf8')

console.log(`New Version: ${version}`)