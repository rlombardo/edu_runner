// const zipFolder = require('zip-folder');
// const AWS = require( 'aws-sdk' );
var fs = require( 'fs' );
var mime = require( 'mime' );

/*AWS.config.region = 'ap-northeast-2';
const DeployBucket = 'elasticbeanstalk-ap-northeast-2-099075676149';	
const version = require('../src/config/version.json').version.toString();
const uploadedArchiveName = 'dt_'+version+'.zip';*/

// const eb = new AWS.ElasticBeanstalk();

const vinyl_fs = require( 'vinyl-fs' );
const ftp = require( 'vinyl-ftp' );
const ftp_creds = require( './utils/ftp_creds.json' );

const callback = function(err) {
    if(err) console.log(err.message)
    else {
        console.log("Files Deployed")
        console.log("Invalidating Distribution...")
        invalidateDistribution(CloudFrontDistributionID, InvalidationPaths, function(err) {
            if(err) console.log(err)
            else {
                console.log("Deployment invalidated")
                console.log("****Deployment Successful!!****")
            }
        })
    }
}

const uploadClient = function() {
	console.log( 'Uploading client...' );

	var ftpClient = ftp.create({
		host: 'wakefield-studios.com',
		port: 21,
		user: ftp_creds.username,
		password: ftp_creds.password,
		log: console.log
	}/*, {		
		logging: 'debug',
		overwrite: 'older'
	}*/);

	const remoteRoot = '/public_html/edu_runner';

	vinyl_fs.src( ['build/bundle.js'], {buffer:false} )
		.pipe( ftpClient.newer(remoteRoot) )
		.pipe( ftpClient.dest(remoteRoot) );

	vinyl_fs.src( ['index.html'], {buffer:false} )
		.pipe( ftpClient.newer(remoteRoot) )
		.pipe( ftpClient.dest(remoteRoot) );

	vinyl_fs.src( ['favicon.ico'], {buffer:false} )
		.pipe( ftpClient.newer(remoteRoot) )
		.pipe( ftpClient.dest(remoteRoot) );

	vinyl_fs.src( ['assets/img/spritesheets/**'], {buffer:false} )
		.pipe( ftpClient.newer(remoteRoot+'/assets/img/spritesheets') )
		.pipe( ftpClient.dest(remoteRoot+'/assets/img/spritesheets') );

	vinyl_fs.src( ['assets/img/ui/**'], {buffer:false} )
		.pipe( ftpClient.newer(remoteRoot+'/assets/img/ui') )
		.pipe( ftpClient.dest(remoteRoot+'/assets/img/ui') );

	vinyl_fs.src( ['assets/text/**'], {buffer:false} )
		.pipe( ftpClient.newer(remoteRoot+'/assets/text') )
		.pipe( ftpClient.dest(remoteRoot+'/assets/text') );

    vinyl_fs.src( ['assets/audio/**'], {buffer:false} )
        .pipe( ftpClient.newer(remoteRoot+'/assets/audio') )
        .pipe( ftpClient.dest(remoteRoot+'/assets/audio') );
}
uploadClient();

/*const updateEBEnvironment = function() {
	const envParams = {
		EnvironmentName: 'dungeonTeam-env',
		VersionLabel: version
	}

	eb.updateEnvironment( envParams, function(err) {
		if( err ) callback( err )
		else {
			console.log( 'EB environment updated!' );
			console.log();
			uploadClient();
		}
	});
}*/

/*const createEBApplicationVersion = function() {
	 const creationParams = {
	 	ApplicationName: 'dungeon_team',
    	VersionLabel: version,
    	SourceBundle: {
    		S3Bucket: DeployBucket,
    		S3Key: uploadedArchiveName
    	}
    }

    eb.createApplicationVersion( creationParams, function(err) {
    	if( err ) {
    		if( err.message.indexOf('already exists') !== -1 ) {
    			console.log( 'Application version already exists.' );
    			uploadClient();
    		} else {
    			callback( err );
    		}
    	}
    	else {
    		console.log( 'EB Application Created!' );
    		console.log();
    		console.log( 'Updating EB environment...' );
    		updateEBEnvironment();
		}
    });	
}*/

/*const uploadToS3 = function() {
    var s3 = new AWS.S3();
    s3.putObject({ Bucket:DeployBucket, Key:uploadedArchiveName, ContentType:mime.lookup('build/dt.zip'), ACL:'public-read', Body:fs.readFileSync('build/dt.zip') }, function(err, res) {
        if(err) callback(err)
        else {
            console.log(`Uploaded: ${uploadedArchiveName}`);
            console.log();
            console.log( 'Creating new EB application version...' );
            createEBApplicationVersion();     
        }
    });
}*/
 
/*zipFolder('src/server', 'build/dt.zip', function(err) {
    if(err) {
        console.log('zip-folder error: ', err);
    } else {
        console.log( 'server directory zipped into build/dt.zip' );
        console.log();
        console.log( 'deploying to S3...');
        uploadToS3();
    }
});*/

