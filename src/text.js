const Config = require( 'constants/Config' );

const text = {}
switch( Config.LOCALE ) {
	
	case 'en':
	default:
		text.ui = require( '../assets/text/en/ui' );
		break;
	
	/*case 'kr': 
		text.ability_trees = require( '../assets/text/kr/ability_trees' ); 
		text.abilities = require( '../assets/text/kr/abilities' ); 
		break;*/
}
module.exports = text;
