import assign from 'object-assign';
import { EventEmitter } from 'events';
import Screens from 'constants/Screens';
import Config from 'constants/Config';
import ApplicationDispatcher from 'flux/dispatchers/ApplicationDispatcher';

// the stuff we serve:
var currentUIScreen = Screens.GAME;

const ApplicationStore = assign( {}, EventEmitter.prototype, {
    ASSETS_LOADED: 'ASSETS_LOADED',
    UI_NAV: 'UI_NAV',
    PLAY_AGAIN: 'PLAY_AGAIN',

    getAll: function() {
    	return {
    		currentUIScreen
    	};
    }
});
module.exports = ApplicationStore;

const HANDLERS = {
    [ApplicationDispatcher.STARTUP]: startup,
    [ApplicationDispatcher.ASSETS_LOADED]: onAssetsLoaded,
    [ApplicationDispatcher.UI_NAV]: onUINav,
    [ApplicationDispatcher.PLAY_AGAIN]: onPlayAgain
}; 

ApplicationDispatcher.register( function(payload) {
    if( HANDLERS[payload.action.actionType] ) {
        HANDLERS[payload.action.actionType]( payload.action );
    }
    return true;
});

function startup() {
   
}

function onAssetsLoaded() {
    ApplicationStore.emit( ApplicationStore.ASSETS_LOADED );
}

function onUINav( action ) {
    currentUIScreen = action.screenIndex;
    ApplicationStore.emit( ApplicationStore.UI_NAV );
}

function onPlayAgain( action ) {
  ApplicationStore.emit( ApplicationStore.PLAY_AGAIN );
}

/*function keyboard(keyCode) {
  var key = {};
  key.code = keyCode;
  key.isDown = false;
  key.isUp = true;
  key.press = undefined;
  key.release = undefined;
  //The `downHandler`
  key.downHandler = function(event) {
    if (event.keyCode === key.code) {
      if (key.isUp && key.press) key.press();
      key.isDown = true;
      key.isUp = false;
    }
    event.preventDefault();
  };

  //The `upHandler`
  key.upHandler = function(event) {
    if (event.keyCode === key.code) {
      if (key.isDown && key.release) key.release();
      key.isDown = false;
      key.isUp = true;
    }
    event.preventDefault();
  };

  //Attach event listeners
  window.addEventListener(
    "keydown", key.downHandler.bind(key), false
  );
  window.addEventListener(
    "keyup", key.upHandler.bind(key), false
  );
  return key;
}*/