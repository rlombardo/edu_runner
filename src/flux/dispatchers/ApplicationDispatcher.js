var Dispatcher = require('flux').Dispatcher;
var assign = require( 'object-assign' );

var ApplicationDispatcher = assign( new Dispatcher(), {
	NAME: 'APPLICATION_DISPATCHER',

	STARTUP: 'STARTUP',
	ASSETS_LOADED: 'ASSETS_LOADED',
	UI_NAV: 'UI_NAV',

    handleAppAction: function(action) {
        this.dispatch({
            source: ApplicationDispatcher.NAME,
            action
        });
    }
});
module.exports = ApplicationDispatcher;