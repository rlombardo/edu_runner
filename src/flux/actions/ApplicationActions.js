var ApplicationDispatcher = require( 'flux/dispatchers/ApplicationDispatcher' );

var ApplicationActions = {
      
    startup: function() {
        ApplicationDispatcher.handleAppAction({
            actionType: ApplicationDispatcher.STARTUP
        });
    },

    assetsLoaded: function() {
        ApplicationDispatcher.handleAppAction({
            actionType: ApplicationDispatcher.ASSETS_LOADED
        });
    },

    uiNav: function( screenIndex ) {
        ApplicationDispatcher.handleAppAction({
            actionType: ApplicationDispatcher.UI_NAV,
            screenIndex
        });
    },

    playAgain: function() {
        ApplicationDispatcher.handleAppAction({
            actionType: ApplicationDispatcher.PLAY_AGAIN
        });
    }
}
module.exports = ApplicationActions;