const Speeds = {};

Speeds.SKY = 0;
Speeds.SUN = 0;
Speeds.BACK_GREY_BUILDINGS = 0.3;
Speeds.MID_GREY_BUILDINGS = 0.6;
Speeds.FRONT_GREY_BUILDINGS = 0.9;
Speeds.COLOR_BUILDINGS = 1.2;
Speeds.TREES = 2.1;
Speeds.GROUND = 2.7;

module.exports = Speeds;