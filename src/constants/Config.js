var _ = require( 'lodash' );

module.exports = {
    SERVER_ADDRESS: process.env.NODE_ENV==='development'? 'http://localhost:80':'http://dungeonteam-env.ap-northeast-2.elasticbeanstalk.com', // 'http://www.dungeonteam-server.xyz', //
    AUTO_LOGIN: false,

    PLATFORM: getUrlVars()['platform'],
    LOCALE: getLocale()
};

// Read a page's GET URL variables and return them as an associative array.
function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function getLocale() {
    // These window.navigator contain language information
    // 1. languages -> Array of preferred languages (eg ["en-US", "zh-CN", "ja-JP"]) Firefox^32, Chrome^32
    // 2. language  -> Preferred language as String (eg "en-US") Firefox^5, IE^11, Safari, 
    //                 Chrome sends Browser UI language
    // 3. browserLanguage -> UI Language of IE
    // 4. userLanguage    -> Language of Windows Regional Options
    // 5. systemLanguage  -> UI Language of Windows
    var browserLanguagePropertyKeys = ['languages', 'language', 'browserLanguage', 'userLanguage', 'systemLanguage'];

    var availableLanguages = ['en'];

    var detectedLocale = _.chain(window.navigator)
        .pick(browserLanguagePropertyKeys) //Get only language properties
        .values() //Get values of the properties
        .flatten() //flatten all arrays
        .compact() //Remove undefined values
        .map(function (x) {
            return x.substr(0, 2); //Shorten strings to use two chars (en-US -> en)
        })
        .find(function (x) {
            return _.includes(availableLanguages, x); //Returns first language matched in available languages
        })
        .value();

    return detectedLocale || 'en'; //If no locale is detected, fallback to 'en'
}
