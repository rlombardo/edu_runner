import { Howl } from 'howler';

let sounds = {
	// background music & ambience
	/*bgm_title: new Howl( {src:['assets/audio/bg/BGM_Title.mp3'], loop:true} ),
	bgm: new Howl( {src:['assets/audio/bg/BGM.mp3'], loop:true} ),
	green_dragon_bgm: new Howl( {src:['assets/audio/bg/BGM_GreenDragonMode.mp3'], loop:true} ),

	// game sound effects
	collided: new Howl( {src:['assets/audio/SFX_Collided.wav']} ),
	score: new Howl( {src:['assets/audio/SFX_score_1.wav']} ),
	new_record: new Howl( {src:['assets/audio/SFX_Notice_Newrecord.wav']} ),
	green_dragon_start: new Howl( {src:['assets/audio/SFX_GreenDragonMode_Start.wav']} ),
	green_dragon_end: new Howl( {src:['assets/audio/SFX_GreenDragonMode_End.wav']} ),
	game_over: new Howl( {src:['assets/audio/SFX_Notice_Gameover.wav']} ),	
	play_again: new Howl( {src:['assets/audio/SFX_Notice_Restart.wav']} )*/
}

// let SettingsStore = require( 'flux/stores/SettingsStore' );

let currentBGTrack = null;
let musicVolume = 1; // SettingsStore.getAll().musicVolume;
let sfxVolume = 1; //  SettingsStore.getAll().sfxVolume;

let lastPlayed = {};

module.exports = {

	play: function( sound, isMusic=false ) {
		var audio = sounds[sound];
		if( !audio /*|| new Date().getTime()-lastPlayed[sound]<100*/ ) {
			return;
		}

		audio.currentTime = 0;
		audio.volume( isMusic? musicVolume:sfxVolume );
		try {
			audio.play();
			lastPlayed[ sound ] = new Date().getTime();
		} catch( e ) {
			console.log( e );
		}
	},

	stop: function( sound ) {
	    if (sounds[sound]) {
	        sounds[sound].pause();
	    }
	},

	setBGTrack: function( sound ) {
		if( currentBGTrack === sound ) {
			return;
		}

		module.exports.stop( currentBGTrack );

		if( sound ) {
			module.exports.play( sound, true )
			sounds[sound].loop = true;
			sounds[sound].currentTime = 0;
		}

		currentBGTrack = sound;
	},

	setMusicVolume: function( value ) {
		musicVolume = value;

		if( sounds[currentBGTrack] ) {
			sounds[currentBGTrack].volume( musicVolume );
		}
	},

	setSFXVolume: function( value ) {
		sfxVolume = value;
		module.exports.play( 'bless' );
	},

	getMusicVolume: function() {
		return musicVolume;
	},

	getSFXVolume: function() {
		return sfxVolume;
	}
}