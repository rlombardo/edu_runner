// Task 1.a - Data Types
// ========
// Assign data of the correct type to each variable.
//
var number = 9999; 
var string = 'fart';
var boolean = true;
// End Task 1.a



// Task 1.b - Operators 
// ========
// Make each comparison evaluate to true
//
var i_b1 = 10 > 2;
var i_b2 = 1 <= 10;
var i_b3 = true === true;
var i_b4 = 24 !== 23;
// End Task 1.b



// Task 1.c - if Statements 
// ========
// Edit the following if Statements so that iLoveCoding ends up true
//
var iLoveCoding;

if( number > 5000 ) {
	iLoveCoding = true;
} else {
	iLoveCoding = false;
}

if( iLoveCoding ) {
	if( string !== 'fart' ) {
		iLoveCoding = false;
	} else {
		iLoveCoding = true;
	}
}

if( iLoveCoding ) {
	if( boolean ) {
		iLoveCoding = true;
	} else {
		iLoveCoding = false;
	}
}
// End of task 1.c



// Task 1.d - Variables
// ========
// The letters a, b, and c are 3 copies of the same background image (this function gets called 8 times - 1 for each layer).
// Each has a position variable (x,y), and variables for height and width.
// We need to position b to the right of a, and c to the right of b.
//
var positionBackgrounds = function( a, b, c ) {
	b.x = a.x + a.width;
    c.x = b.x + b.width;
} 
// End Task 1.d



// Task 2.a - Functions (calling) 
// ========
// When the background images fall off the left, we need to reposition them back on the right.
// The function named checkForBGOffLeft does that, but we need to call it.
// Inside the ii_a function, call the checkForBGOffLeft function.
//
var ii_a = function( a, b, c ) {

	var checkForBGOffLeft = function() {
		if( a.x+a.width < 0 ) {
	        a.x = c.x + c.width;
	    } else if( b.x+b.width < 0 ) {
	        b.x = a.x + a.width;
	    } else if( c.x+c.width < 0 ) {
	        c.x = b.x + b.width;
	    }
	}

	checkForBGOffLeft();
}
// End Task 2.a



// Task 2.b - Functions (return)  
// ========
// The background is moving too fast!
// The correct speeds are included in the functions below, but something is missing. Fix it!
//
var getLayer0Speed = function() {
	return 0;
}

var getLayer1Speed = function() {
	return 0;
}

var getLayer2Speed = function() {
	return 0.3;
}

var getLayer3Speed = function() {
	return 0.6;
}

var getLayer4Speed = function() {
	return 0.9;
}

var getLayer5Speed = function() {
	return 1.2;
}

var getLayer6Speed = function() {
	return 2.1;
}

var getLayer7Speed = function() {
	return 2.7;
}
// End Task 2.b



// Task 2.c - Functions (parameters)  
// ========
// Without changing the robertIsTheCoolestTeacher function, make it return true.
//
var robertIsTheCoolestTeacher = function( hePaidMeToSayThis, iAmNotLying ) {
	return hePaidMeToSayThis===false && iAmNotLying===true;
}

var twoC = robertIsTheCoolestTeacher( false, true /*HINT: something goes here*/ );
// End Task 2.c



// Task 2.d - Functions (scope)  
// ========
// What 4 values should the console show? (needs to be in the correct order)
//
var a = 6;
function test() {	
	
	var a = 7;    
    console.log( a );
    
    var again = function() {        
        var a = 8;        
        console.log( a );
    }
    
    again();    
    console.log( a );
}
a = 9;
test();
console.log( a );

// answers go here:
var ii_d_FIRST = 7;
var ii_d_SECOND = 8;
var ii_d_THIRD = 7;
var ii_d_FOURTH = 9;
// End Task 2.d



// Task 3 - Review  
// ========
// The following function may take input of any data type.
// If the input is a boolean, it should return the opposite of input value.
// If the input is a number, it should return the number multiplied by 3.
// If the input is a string, it should return the reverse of that string.
// HINT: If you get stuck, try googling 'javascript typeof'
//
var iii = function( input ) {
	if( typeof input === 'boolean' ) {
		return !input;
	} else if( typeof input === 'number' ) {
		return input * 3;
	} else if( typeof input === 'string' ) {
		return input.split('').reverse().join('');
	}
}
// End Task 3



// Task 4 - for loops 
// ======
// The doMonsterSpawn function takes a parameter function that spawns 1 monster.
// Rewrite this function so that it spawns 3 monsters at a time. (repeating the call 3 times is not allowed)
//
var doMonsterSpawn = function( monsterSpawnFunction ) {
	for( var i = 0; i < 3; ++i ) {
		monsterSpawnFunction();
	}
}
// End Task 4



// Task 5 - for Loops and Arrays
// ======
// Let's give the game a more apocolyptic feel.
//
// The following function acccpets an array that contains all the background images.
// Color each one red by setting its tint property to 0xaa5555, as in "img.tint = 0xaa5555"
// 
// Remember that there are three copies of each image, so changing one layer should look like this
// layer.a.tint = 0xaa5555;
// layer.b.tint = 0xaa5555;
// layer.c.tint = 0xaa5555;
//
var doBGTint = function( bgLayers ) {
	for( var i = 0; i < bgLayers.length; ++i ) {
        const TINT = 0xaa5555;
        bgLayers[i].a.tint = TINT;            
        bgLayers[i].b.tint = TINT;
        bgLayers[i].c.tint = TINT;
    }
}
// End Task 5



// Task 6 - Adding Data to an Array
// ======
// Let's start fighting back.
//
// The fireLaserFunction ... well, fires a laser.
// abilities is an array holding all the runner's abilities.
// Add the fireLaserFunction to the abilities array, and return the array.
//
// If it works, pressing spacebar will fire a laserbeam!
//
var addAbility = function( abilities, fireLaserFunction ) {
	abilities.push( fireLaserFunction );
	return abilities;
}


// Task 7 - Just for fun
// ======
// We need to jump to hit those flying enemies!
//
// Note that all time measurements are in seconds.
// (You may want to turn off monster spawning while you test this.)
//
var getJumpHeight = function() {
	return 300;
}

var getAirTime = function() {
	return 1.5;
}

var getUpDelay = function() {
	return 0.12;
}


var getDownDelay = function() {
	return 0.12;
}
// End Task 7



// Task 8 - while Loops
// ======
// Now we need to detect collisions between laser beams and monsters.
//
// laserBeam is one laser beam sprite, and monsters is an array holding all the monster sprites
// isColliding is a function that accepts 2 sprite parameters, and checks whether they are overlapping each other
// The onCollisionFunction also accepts 2 sprite parameters, and needs to be called whenever isColliding returns true
//
// Write a while loop that checks for a collision between laserBeam and ANY monster.
// It should exit the loop if it finds a collision
//
var detectCollision = function( laserBeam, monsters, isColliding, onCollisionFunction ) {
	var i = 0;
	var foundCollision = false;
	while( i < monsters.length && !foundCollision ) {
		if( isColliding(laserBeam,monsters[i]) ) {
			onCollisionFunction( laserBeam, monsters[i] );
			foundCollision = true;
		}

		++i;
	}
}
// End Task 8



// Task 9 - switch Statements
// ======
// Let's spice things up a little bit with a 2nd monster type
// Write a swicth statement that will randomly choose which spawn function to call
//
var chooseSpawnType = function( spawnFlyingCrusher, spawnGreenMonster ) {
	switch( Math.floor(Math.random()*2) ) {		
		case 1: spawnGreenMonster(); break;
		case 0: spawnFlyingCrusher(); break;
	}	
}
// End Task 9 














































































module.exports = {
	i_a: () => {
		return 		typeof number === 'number'
				&&	typeof string === 'string'
				&&	typeof boolean === 'boolean'
	},

	i_b: () => {
		return i_b1 && i_b2 && i_b3 && i_b4;
	},

	i_c: () => {
		return iLoveCoding;
	},

	i_d: ( bgTrio ) => {
		positionBackgrounds( bgTrio.a, bgTrio.b, bgTrio.c );
	},

	checkForBGOffLeft: ( bgTrio ) => {
		ii_a( bgTrio.a, bgTrio.b, bgTrio.c );
	},

	getBGSpeeds: () => {
		const Speeds = {};
		Speeds.SKY 					= getLayer0Speed() || 0;
		Speeds.SUN 					= getLayer1Speed() || 0;
		Speeds.BACK_GREY_BUILDINGS 	= getLayer2Speed() || 10.9;
		Speeds.MID_GREY_BUILDINGS 	= getLayer3Speed() || 11.8;
		Speeds.FRONT_GREY_BUILDINGS = getLayer4Speed() || 12.7;
		Speeds.COLOR_BUILDINGS 		= getLayer5Speed() || 13.6 ;
		Speeds.TREES 				= getLayer6Speed() || 16.3;
		Speeds.GROUND 				= getLayer7Speed() || 18.1;

		return Speeds;
	},

	ii_c: () => {
		return twoC === true;

	},

	ii_d: () => {
		return ii_d_FIRST===7 && ii_d_SECOND===8 && ii_d_THIRD===7 && ii_d_FOURTH===9;
	},

	getMonsterSpawnDelay: () => {
		 if(	iii('fart')==='traf' 
			&&	iii(false)===true && iii(true)===false
			&&	iii(5)===15 && iii(11)===33 
		) {
			return 3.0;
		} else {
			return null
		};
	},

	iv: ( monsterSpawnFunction ) => {
		return doMonsterSpawn( monsterSpawnFunction );
	},

	v: ( bgLayers ) => {
		return doBGTint( bgLayers );
	},

	vi: () => {
		var result = addAbility( ['a','b','c'], 'd' )
		return result.length===4 && result[3]==='d';
	},

	vii: ( runnerState, States ) => {
		return {
			jumpHeight: getJumpHeight(),
			upDuration: getAirTime() / 2,
			upDelay: getUpDelay(),
			downDelay: getDownDelay()
		}
	},

	viii: ( laserBeam, monstersDict, collisionCheck, onCollision ) => {
		var monstersArray = [];
		for( var prop in monstersDict ) {
			monstersArray.push( monstersDict[prop] );
		}

		detectCollision( laserBeam, monstersArray, collisionCheck, onCollision );
	},

	ix: ( spawnFlyingCrusher, spawnGreenMonster ) => {
		return chooseSpawnType( spawnFlyingCrusher, spawnGreenMonster );
	}

}