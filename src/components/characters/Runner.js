import ApplicationStore from 'flux/stores/ApplicationStore';
import Test from 'Test';

var RUNNING_FRAMES;
var JUMPING_TEXTURE;
var JUMPING_TEXTURE_2;
var FALLING_TEXTURE;
const onAssetsLoaded = () => {
    RUNNING_FRAMES = [ 
        PIXI.Texture.fromFrame( 'angry_runner/obj0.png' ),
        PIXI.Texture.fromFrame( 'angry_runner/obj1.png' ),
        PIXI.Texture.fromFrame( 'angry_runner/obj2.png' ),
        PIXI.Texture.fromFrame( 'angry_runner/obj3.png' ),
        PIXI.Texture.fromFrame( 'angry_runner/obj4.png' ),
        PIXI.Texture.fromFrame( 'angry_runner/obj5.png' ),
        PIXI.Texture.fromFrame( 'angry_runner/obj6.png' ),
        PIXI.Texture.fromFrame( 'angry_runner/obj7.png' )
    ];

    JUMPING_TEXTURE = PIXI.Texture.fromFrame( 'angry_runner/jump1.png' );
    JUMPING_TEXTURE_2 = PIXI.Texture.fromFrame( 'angry_runner/jump2.png' );
    FALLING_TEXTURE = PIXI.Texture.fromFrame( 'angry_runner/jump3.png' );
}
ApplicationStore.on( ApplicationStore.ASSETS_LOADED, onAssetsLoaded );

const Runner = function() {
    PIXI.Container.call( this );

    var _done = false;

    const States = {
        RUNNING: 'RUNNING',
        JUMPING: 'JUMPING',
        JUMPING_2: 'JUMPING_2',
        FALLING: 'FALLING'
    }
    var _state = States.RUNNING;

    var _jumpTween;

    this.dispose = () => {
        if( _jumpTween ) {
            _jumpTween.kill();
        }

        this.removeChildren();
        _done = true;
    }

    this.jump = () => {
        if( _state===States.JUMPING || _state===States.JUMPING_2 || _state===States.FALLING ) {
            return;
        }

        if( _jumpTween ) {
            _jumpTween.kill();
        }

        var jumpData = Test.vii( _state, States );
        
        _jumpTween = TweenMax.to( this, jumpData.upDuration, {
            y: this.y - jumpData.jumpHeight,
            ease: Quad.easeOut,
            yoyo: true,
            repeat: 1,
            onRepeat: () => {
                TweenMax.delayedCall( jumpData.downDelay, () => {
                    _state = States.FALLING;
                });
            },
            onComplete: () => {
                _state = States.RUNNING;
            }
        });
        TweenMax.delayedCall( jumpData.upDelay, () => {
            _state = States.JUMPING_2;
        });
        
        _state = States.JUMPING;
    }

    var base = PIXI.Sprite.fromFrame( 'angry_runner/obj7.png' );    
    base.anchor.x = base.anchor.y = 0.5;
    this.addChild( base );
     
    var _runningFrame = 0;
    var _runningFrameMax = 7;
    const animate = () => {
        if( _state===States.RUNNING ) {
            base.texture = RUNNING_FRAMES[ ++_runningFrame ];
            if( _runningFrame >= _runningFrameMax ) {
                _runningFrame = 0;
            } 
        } else if( _state===States.JUMPING ) {
            base.texture = JUMPING_TEXTURE;
        } else if( _state===States.JUMPING_2 ) {
            base.texture = JUMPING_TEXTURE_2;
        } else if( _state===States.FALLING ) {
            base.texture = FALLING_TEXTURE;
        } else {
            base.texture = RUNNING_FRAMES[ 0 ];
        }

        TweenMax.killDelayedCallsTo( animate );
        TweenMax.delayedCall( 0.06, animate );
    }

    if( Test.ii_d() ) {
        TweenMax.delayedCall( 0.06, animate );
    }
}
Runner.prototype = Object.create( PIXI.Container.prototype );
Runner.prototype.constructor = Runner;
module.exports = Runner;