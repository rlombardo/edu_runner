import ApplicationStore from 'flux/stores/ApplicationStore';

var WALKING_FRAMES;
const onAssetsLoaded = () => {
    WALKING_FRAMES = [ 
        PIXI.Texture.fromFrame( 'green_monster/1.png' ),
        PIXI.Texture.fromFrame( 'green_monster/2.png' ),
        PIXI.Texture.fromFrame( 'green_monster/3.png' ),
        PIXI.Texture.fromFrame( 'green_monster/4.png' ),
        PIXI.Texture.fromFrame( 'green_monster/5.png' ),
        PIXI.Texture.fromFrame( 'green_monster/6.png' )
    ];
}
ApplicationStore.on( ApplicationStore.ASSETS_LOADED, onAssetsLoaded );

const GreenMonster = function() {
    PIXI.Container.call( this );

    var _done = false;

    const States = {
        WALKING: 'WALKING',
    }
    var _state = States.WALKING;

    this.transitionIn = () => {
        
    }

    this.transitionOut = ( onComplete ) => {  

    }

    this.dispose = () => {
       this.removeChildren();
       TweenMax.killDelayedCallsTo( animate );
        _done = true;
    }

    var base = PIXI.Sprite.fromFrame( 'green_monster/1.png' );    
    base.anchor.x = base.anchor.y = 0.5;
    this.addChild( base );
     
    var _walkingFrame = 0;
    var _walkingFrameMax = 5;
    const animate = () => {
        if( _state===States.WALKING ) {
            base.texture = WALKING_FRAMES[ ++_walkingFrame ];
            if( _walkingFrame >= _walkingFrameMax ) {
                _walkingFrame = 0;
            } 
        }

        if( !_done ) {
            TweenMax.delayedCall( 0.06, animate );
        }
    }
    TweenMax.delayedCall( 0.06, animate );    
}
GreenMonster.prototype = Object.create( PIXI.Container.prototype );
GreenMonster.prototype.constructor = GreenMonster;
module.exports = GreenMonster;