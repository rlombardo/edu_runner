import React from 'react';
import Screens from 'constants/Screens';
import CanvasTools from 'components/CanvasTools';
import ApplicationActions from 'flux/actions/ApplicationActions';
import ApplicationStore from 'flux/stores/ApplicationStore';
// import 'css/bootstrap-3.3.6-dist/css/bootstrap.css';
import 'css/global.css';
import Audio from 'Audio';

const ScreensDict = {
    [Screens.GAME]: require( './ui_screens/game/GameScreen' )
}

const App = React.createClass({
    
    getInitialState: function() {
        return {
            assetsLoaded: false,
            uiScreenTag: null,
            done: false
        };    
    },
    
    render: function() {
        if( this.state.assetsLoaded ) {
            this.renderBody();
        }

        return (
            <div className="root-div" id="root-div">
                <div className="game-canvas" id="game-canvas"></div>                
                <div className="overlay-canvas" id="overlay-canvas"></div>
            </div>
        );        
	},

    renderBody: function() {
        const storeScreenTag = ApplicationStore.getAll().currentUIScreen;
        if( this.state.uiScreenTag !== storeScreenTag ) {
            
            const loadNewScreen = () => {
                var ScreenClass = ScreensDict[ storeScreenTag ];        
                this.uiScreen = new ScreenClass();
                this.uiScreen.transitionIn();
                PIXI.stage.addChildAt( this.uiScreen, 0 );

                this.state.uiScreenTag = storeScreenTag;
            } 

            if( this.uiScreen ) {
                this.uiScreen.transitionOut( ()=>{
                    this.uiScreen.dispose();
                    PIXI.stage.removeChild( this.uiScreen );
                    loadNewScreen();   
                });
            } else {
                loadNewScreen();
            }
        }
    },

    onPlayAgain: function() {
        if( this.uiScreen ) {
            this.uiScreen.dispose();
            PIXI.stage.removeChild( this.uiScreen );
            this.onUINav();
        }
    },

    componentWillMount: function() {
        ApplicationActions.startup();

        ApplicationStore.on( ApplicationStore.UI_NAV, this.onUINav );
        ApplicationStore.on( ApplicationStore.PLAY_AGAIN, this.onPlayAgain );
    },
    
    componentDidMount: function() {
        this.makePixiRenderer();

        window.addEventListener( "resize", this.handleResize );
        window.addEventListener( "orientationchange", this.handleResize );  
        this.handleResize(); 

        // perform some cleanup when the window closes
        window.onbeforeunload = () => {
            Audio.setBGTrack( null );

            PIXI.stage.removeChildren();
            PIXI.stage.destroy();
            PIXI.stage = null;

            ApplicationStore.removeListener( ApplicationStore.UI_NAV, this.onUINav );
            ApplicationStore.removeListener( ApplicationStore.PLAY_AGAIN, this.onPlayAgain );

            this.state.done = true;
        };
    },

    handleResize( value, e ) {
        PIXI.WIDTH = window.innerWidth;
        PIXI.HEIGHT = window.innerHeight;
        PIXI.stage._width = window.innerWidth;
        PIXI.stage._height = window.innerHeight;
        PIXI.gameRenderer.resize( window.innerWidth, window.innerHeight );
        PIXI.overlay._width = window.innerWidth;
        PIXI.overlay._height = window.innerHeight;
        PIXI.overlayRenderer.resize( window.innerWidth, window.innerHeight );        
        // PIXI.stagePadding = Math.round( PIXI.stage._height*0.02 );
        
        if( this.uiScreen ) {
            this.uiScreen.dispose();
            PIXI.stage.removeChild( this.uiScreen );
            this.onUINav();
        }
    },

    onUINav: function() {
        this.setState({
            uiScreenTag: null
        });
    },

    makePixiRenderer: function() {
        PIXI.gameRenderer = new PIXI.autoDetectRenderer( 
            window.innerWidth, 
            window.innerHeight, 
            {
                transparent: true, // backgroundColor: Colors.ALMOST_BLACK, 
                antialias: true,
                resolution: window.devicePixelRatio || 1,
                autoResize: true,
                roundPixels: false
            } 
        );

        PIXI.overlayRenderer = new PIXI.autoDetectRenderer( 
            window.innerWidth, 
            window.innerHeight, 
            {
                transparent: true, 
                antialias: true,
                resolution: window.devicePixelRatio || 1,
                autoResize: true,
                roundPixels: true
            } 
        );

        // insert PIXI canvas elements into the DOM.
        document.getElementById('game-canvas').appendChild( PIXI.gameRenderer.view );
        document.getElementById('overlay-canvas').appendChild( PIXI.overlayRenderer.view );
        
        // root containers that will hold the scenes we draw.
        PIXI.stage = new PIXI.Container();        
        PIXI.stage._width = window.innerWidth;
        PIXI.stage._height = window.innerHeight;
        // PIXI.stagePadding = Math.round( PIXI.stage._height*0.02 );

        PIXI.overlay = new PIXI.Container();        
        PIXI.overlay._width = window.innerWidth;
        PIXI.overlay._height = window.innerHeight;

        PIXI.loader.add([ 
            'assets/img/spritesheets/ss0.json',
            'assets/img/spritesheets/ss1.json',
            'assets/img/spritesheets/ss2.json',
            'assets/img/spritesheets/ss3.json',
            'assets/img/spritesheets/ss4.json'

        ]).load( this.onAssetsLoaded );
    },

    onAssetsLoaded: function() {
        ApplicationActions.assetsLoaded();
        this.setState( {assetsLoaded:true} );

        const update = () => {
            PIXI.gameRenderer.render( PIXI.stage );
            PIXI.overlayRenderer.render( PIXI.overlay );

           /* if( PIXI.overlay.children.length ) {
                $('#overlay-canvas').css( 'pointer-events', 'all' );
            } else {                
                $('#overlay-canvas').css( 'pointer-events', 'none' );
            }*/

            if( !this.state.done ) {
                requestAnimationFrame( update );
            }   
        }
        update();
    }
});
module.exports = App;