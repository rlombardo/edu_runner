const CanvasTools = {	

    dynamicFontSizeString: function( maxSize ) {
        return Math.max( 6, Math.min( maxSize, maxSize*(PIXI.stage._width/1000) ) ).toString()+'px'
    },

    addBackFill: function( container, color ) {
        var bg = new PIXI.Graphics();
        bg.beginFill( color );
        bg.drawRect( 0, 0, container._width, container._height );
        bg.endFill();
        container.addChild( bg );
        return bg;
    },

    addBorder: function( container, topTexPath, rightTexPath, bottomTexPath, leftTexPath, padding ) {
        if( container.hasBorder ) {
            return;
        }

        container.borderTiles = [];

        var tile1, tile2;
        var texPath;

        if( topTexPath || bottomTexPath ) {
            // do horizontal border tiling
            const widthToTile = container._width - padding*2;
            var tiledWidth = 0;

            const addHorizTile = ( top ) => {
                if( top ) {
                    texPath = topTexPath;
                } else {
                    texPath = bottomTexPath;
                } 

                if( !texPath ) {
                    return null;
                }

                var tile = new PIXI.Sprite.fromFrame( texPath );
                tile.x = Math.round( padding + tiledWidth );
                tile.y = Math.round( top? padding : container._height - padding - tile.height );
                container.addChild( tile );
                container.borderTiles.push( tile );
                return tile;
            }

            const addHorizMask = ( tile ) => {
                var mask = new PIXI.Graphics();
                mask.drawRect( 0, -tile.height/2, tile.width-tiledWidth+widthToTile, tile.height*2 );
                tile.addChild( mask );
                tile.mask = mask;
            }

            while( tiledWidth < widthToTile ) {
                tile1 = addHorizTile( true );
                tile2 = addHorizTile( false );

                tiledWidth += tile1.width;

                if( tiledWidth > widthToTile ) {
                    if( topTexPath ) addHorizMask( tile1 );
                    if( bottomTexPath ) addHorizMask( tile2 )
                }
            }
        }

        if( leftTexPath || rightTexPath ) {
            // do vertical border tiling
            const heightToTile = container._height - padding*2;
            var tiledHeight = 0;

            const addVertTile = ( left ) => {
                if( left ) {
                    texPath = leftTexPath;
                } else {
                    texPath = rightTexPath;
                } 

                if( !texPath ) {
                    return null;
                }

                var tile = new PIXI.Sprite.fromFrame( texPath );
                tile.x = Math.round( left? padding : container._width - padding - tile.width );
                tile.y = Math.round( padding + tiledHeight );
                container.addChild( tile );
                container.borderTiles.push( tile );
                return tile;
            }

            const addVertMask = ( tile ) => {
                var mask = new PIXI.Graphics();
                mask.drawRect( -tile.width/2, 0, tile.width*2, tile.height-tiledHeight+heightToTile );
                tile.addChild( mask );
                tile.mask = mask;
            }

            while( tiledHeight < heightToTile ) {
                tile1 = addVertTile( true );
                tile2 = addVertTile( false );

                tiledHeight += tile1.height;

                if( tiledHeight > heightToTile ) {
                    if( leftTexPath ) addVertMask( tile1 );
                    if( rightTexPath ) addVertMask( tile2 )
                }
            }
        }

        container.hasBorder = true;
    },

    clearBorder: function( container ) {
        if( container.hasBorder ) {
            for( var i = 0; i < container.borderTiles.length; ++i ) {
                container.removeChild( container.borderTiles[i] );
            }
            container.borderTiles = null;
            container.hasBorder = false;
        }
    },
    
    flipHorizontal: function(obj) {
        if( obj ) {
            obj.scale.x = -obj.scale.x;
        }        
    },

    getResponsiveY: function( coefficient ) {
    	var header = window.document.getElementById( "header" );
        var body = window.document.getElementById( "body" );
        return header.clientHeight + body.clientHeight/2 + coefficient/PIXI.stage._height;
    },

    getTooltipX: function( mouseX, ttWidth ) {
        var ttX = mouseX > PIXI.stage._width/2? mouseX-ttWidth-60 : mouseX+60;

        if( ttX < 15 ) {
            return 15;
        }

        if( ttX + ttWidth > PIXI.stage._width ) {
            return PIXI.stage._width - ttWidth - 15;
        }

        return ttX;
    },

    getTooltipY: function( mouseY, ttHeight ) {
        var ttY = mouseY > PIXI.stage._height/2? mouseY-ttHeight-30 : mouseY+30;

        if( ttY < 5 ) {
            return 5;
        }

        if( ttY + ttHeight > PIXI.stage._height ) {
            var result = PIXI.stage._height - ttHeight - 15;
            return result < 5? 5 : result;
        }

        return ttY;
    },

    angleToVector: function( angleInRadians, magnitude ) {
        var vector = {};
        vector.x = Math.sin( angleInRadians ) * magnitude;
        vector.y = Math.cos( angleInRadians ) * magnitude;
        
        return vector;
    },

     // returns angle in radians
    angleFromSpriteToSprite: function( s1, s2 ) {
        var dx = s2.x - s1.x;
        var dy = s2.y - s1.y;
        // return 180 - Math.atan2( dx,dy ) * 180/Math.PI; // degrees
        return Math.PI - Math.atan2( dx, dy ); 
    },

    distanceFromSpriteToSprite: function( s1, s2 ) {
        var xDist = Math.abs( s1.x - s2.x );
        var yDist = Math.abs( s1.y - s2.y );
        
        return Math.sqrt( xDist*xDist + yDist*yDist );
    }
        
        
}
module.exports = CanvasTools;