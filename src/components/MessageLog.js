const Colors = require( 'constants/Colors' );
const text = require( 'text' );

const MessageLog = () => {

	const onInsufficientStats = ( data ) => {
		const unit = BattleStore.getAll().battleState.allPieces[data.actorId];
		const ability = BattleCalc.getEquippedAbility( unit, data.abilityHandle );

		if( unit.ap < ability.apCost ) {
			MessageLog.addMessage( text.battle.not_enough_ap, Colors.RED );
		} else if( unit.mp < ability.mpCost ) {
			MessageLog.addMessage( text.battle.not_enough_mp, Colors.RED );
		} else {
			MessageLog.addMessage( text.battle.insufficient_stats, Colors.RED );
		}		
	}

	const onIllegalAction = () => {
		MessageLog.addMessage( text.battle.illegal_action, Colors.RED );
	}
}
module.exports = MessageLog;

module.exports.addMessage = ( text, color ) => {
	var message = new PIXI.Text( text, {
		fontFamily: 'Courier New',
		fontSize: '20px',
		// fontWeight: 'bold',
		fill: color
		// dropShadow: true,
		// dropShadowColor: 0xffffff,
		// dropShadowDistance: 1
	});

	message.x = PIXI.stage._width/2 - message.width/2;
	message.y = PIXI.stage._height * 0.2;
	PIXI.stage.addChild( message );

	TweenMax.to( message, 5.0, {y:PIXI.stage._height*0.05, alpha:0} );
}