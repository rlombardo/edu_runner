import Test from 'Test';

const Background = function() {
    PIXI.Container.call( this );

    var _done = false;    

    this.transitionIn = () => {
        
    }

    this.transitionOut = ( onComplete ) => {  

    }

    this.dispose = () => {
        this.removeChildren();

        _done = true;
    }

    const makeBGLayer = ( texPath ) => {
        var layer = PIXI.Sprite.fromFrame( texPath );
        layer.height = PIXI.HEIGHT;
        layer.scale.x = layer.scale.y;
        this.addChild( layer );
        return layer;
    };

    var _layers = [];
    
    if( Test.i_b() ) {

        // UNIT 5 - Arrays & for Loops
        for( var i = 0; i < 8; ++i ) {
            
            // UNIT 11 - Objects
            var bgTrio = {
                a: makeBGLayer( 'bg/layer_'+i+'.png' ),
                b: makeBGLayer( 'bg/layer_'+i+'.png' ),
                c: makeBGLayer( 'bg/layer_'+i+'.png' )
            };

            // Position the backgrounds so they line up one after the other
            Test.i_d( bgTrio );
            
            // set the sccroll speed for each layer of the background
            const Speeds = Test.getBGSpeeds();
            switch( i ) {
                case 0: bgTrio.speed = Speeds.SKY; break;
                case 1: bgTrio.speed = Speeds.SUN; break;
                case 2: bgTrio.speed = Speeds.BACK_GREY_BUILDINGS; break;
                case 3: bgTrio.speed = Speeds.MID_GREY_BUILDINGS; break;
                case 4: bgTrio.speed = Speeds.FRONT_GREY_BUILDINGS; break;
                case 5: bgTrio.speed = Speeds.COLOR_BUILDINGS; break;
                case 6: bgTrio.speed = Speeds.TREES; break;
                case 7: bgTrio.speed = Speeds.GROUND; break;
                default: console.log( "GameScreen - This shouldn't happen." );
            }

            // add the trio of layers to our array
            _layers.push( bgTrio );        
        }
    } else {
        makeBGLayer( 'bg/layer_0.png' );
    }

    Test.v( _layers );

    const loop = () => {
        for( var i = 0; i < _layers.length; ++i ) {
            var layer = _layers[i];

            layer.a.x -= layer.speed;
            layer.b.x -= layer.speed;
            layer.c.x -= layer.speed;

            Test.checkForBGOffLeft( layer );
        }

        if( !_done ) {
            requestAnimationFrame( loop );
        }
    }

    if( Test.i_c() ) {
        requestAnimationFrame( loop );
    }
}
Background.prototype = Object.create( PIXI.Container.prototype );
Background.prototype.constructor = Background;
module.exports = Background;