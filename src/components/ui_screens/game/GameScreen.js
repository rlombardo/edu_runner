import Background from './Background';
import Runner from 'components/characters/Runner';
import GreenMonster from 'components/characters/GreenMonster';
import FlyingCrusher from 'components/characters/FlyingCrusher';
import Test from 'Test';

const GameScreen = function() {
    PIXI.Container.call( this );

    if( !Test.i_a() ) {
        return
    }

    var _done = false;

    this.transitionIn = () => {}
    this.transitionOut = ( onComplete ) => {}

    this.dispose = () => {
        cancelAnimationFrame( _interval );

        _background.dispose();

        this.removeChildren();

        _done = true;
    }

    var _background = new Background();
    this.addChild( _background );

    var _runner = new Runner();
    if( Test.ii_c() ) {
        _runner.x = PIXI.WIDTH * 0.1;
        _runner.y = PIXI.HEIGHT * 0.8;
        this.addChild( _runner );
    }

    const killBeam = ( beamUid ) => {
        if( !_laserBeams[beamUid] ) {
            return;
        }

        if( _laserBeams[beamUid].parent ) {
            this.removeChild( _laserBeams[beamUid] );
        }
        delete _laserBeams[ beamUid ];
    }

    const killEnemy = ( enemyUid ) => {
        if( !_enemies[enemyUid] ) {
            return;
        }

        if( _enemies[enemyUid].parent ) {
            this.removeChild( _enemies[enemyUid] );
        }
        
        _enemies[enemyUid].dispose();
        delete _enemies[ enemyUid ];
    }

    var _beamUid = 0;
    var _laserBeams = {};
    const fireLaser = () => {
        var beam = PIXI.Sprite.fromFrame( 'laser_beam.png' );
        beam.dangerous = true;
        
        // give it a unique identifier & add it to the dictionary
        beam.uid = _beamUid++;
        _laserBeams[ beam.uid ] = beam;

        // scale & position the beam
        beam.width = _runner.width;
        beam.scale.y = beam.scale.x;
        beam.x = _runner.x - _runner.width*0.3  ;
        beam.y = _runner.y - _runner.height*0.28;
        
        // tween it forward
        TweenMax.to( beam, 0.5, {
            x: PIXI.WIDTH + beam.width,
            ease: Linear.easeNone,
            onComplete: killBeam,
            onCompleteParams: [beam.uid]
        }); 

        // tween the x-scale to make it appear to grow
        TweenMax.from( beam.scale, 0.25, {x:0} );

        this.addChild( beam );
    }

    this.interactive = true;
    document.onkeydown = ( event ) => {
        switch( event.keyCode ) {
            case 32: // spacebar
                if( Test.vi() ) {
                    fireLaser();
                }
                break;

            case 38: // up-arrow
                if( Test.vii().jumpHeight ) {
                    _runner.jump();
                }
                break;
        } 
    }

    const spawnGreenMonster = () => {
        var gm = new GreenMonster();

        // give it a unique identifier & add it to the dictionary
        gm.uid = _enemyUid++;
        _enemies[ gm.uid ] = gm;

        // scale & position the monster
        gm.height = _runner.height*1.3;
        gm.scale.x = gm.scale.y;
        gm.x =  PIXI.WIDTH + gm.width + Math.random()*gm.width*1.5;
        gm.y = PIXI.HEIGHT*0.7 + Math.random()*PIXI.HEIGHT*0.15;

        // tween it forward
        TweenMax.to( gm, 9.5, {
            x: -Math.random()*gm.width*5,
            ease: Linear.easeNone,
            onComplete: killEnemy,
            onCompleteParams: [gm.uid]
        });

        var a;
        this.addChild( gm );
    }

    const spawnFlyingCrusher = () => {
        var fc = new FlyingCrusher();

        // give it a unique identifier & add it to the dictionary
        fc.uid = _enemyUid++;
        _enemies[ fc.uid ] = fc;

        // scale & position the monster
        fc.height = _runner.height*0.6;
        fc.scale.x = fc.scale.y;
        fc.x = PIXI.WIDTH + fc.width + Math.random()*fc.width*1.5;
        fc.y = PIXI.HEIGHT*0.1 + Math.random()*PIXI.HEIGHT*0.45;

        // tween it forward
        TweenMax.to( fc, 6.5, {
            x: -Math.random()*fc.width*5,
            ease: Linear.easeNone,
            onComplete: killEnemy,
            onCompleteParams: [fc.uid]
        });

        this.addChild( fc );
    }

    const monsterSpawnFunction = () => {
        Test.ix( spawnFlyingCrusher, spawnGreenMonster );
    }

    var _enemyUid = 0;
    var _enemies = {};
    const DELAY = Test.getMonsterSpawnDelay();
    const spawnEnemy = () => {
        Test.iv( monsterSpawnFunction );
        TweenMax.delayedCall( DELAY/6+Math.random()*(DELAY/2), spawnEnemy );
    }

    if( DELAY ) {
        TweenMax.delayedCall( DELAY/3, spawnEnemy );
    }

    const isIntersecting = ( r1, r2 ) => {
        if( !r1 || !r2 ) {
            return;
        }

        r1 = r1.getBounds();
        r2 = r2.getBounds();

        return   !(r2.x > (r1.x + r1.width)
               || (r2.x + r2.width) < r1.x 
               || r2.y > (r1.y + r1.height)
               || (r2.y + r2.height) < r1.y);

    }

    const collisionCheck = ( laserBeam, enemy ) => {
        return laserBeam.dangerous && isIntersecting( laserBeam, enemy );
    }

    const onCollision = ( laserBeam, enemy ) => {
        killEnemy( enemy.uid );
        TweenMax.delayedCall( 0.03, killBeam, [laserBeam.uid] );
        laserBeam.dangerous = false;
    }

    const loop = () => {

        for( var beamId in _laserBeams ) { 
            Test.viii( _laserBeams[beamId], _enemies, collisionCheck, onCollision )
        }

        _interval = requestAnimationFrame( loop );
    }
    var _interval = requestAnimationFrame( loop )

}
GameScreen.prototype = Object.create( PIXI.Container.prototype );
GameScreen.prototype.constructor = GameScreen;
module.exports = GameScreen;